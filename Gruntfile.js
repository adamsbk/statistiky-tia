module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		constants: {
			tinyPngImgFiles: 'jpg,jpeg,png',
			resPath: 'resources/assets',
			publicPath: 'public'
		},

		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					sourceMap: true,
					cacheLocation: '<%= constants.resPath %>/sass/.sass-cache'
				},
				files: {
					'<%= constants.publicPath %>/css/style.css': '<%= constants.resPath %>/sass/style.scss'
				}
			}
		},

		uglify: {
			'<%= constants.publicPath %>/js/main.js': [
				'<%= constants.resPath %>/js/bootstrap.min.js',
				'<%= constants.resPath %>/js/app.js',
				'<%= constants.resPath %>/js/controller/categoriesController.js',
				'<%= constants.resPath %>/js/controller/foodController.js',
				'<%= constants.resPath %>/js/controller/statsController.js',
				'<%= constants.resPath %>/js/controller/usersController.js'
			]
		},

		watch: {
			css: {
				files: '<%= constants.resPath %>/sass/**/*.scss',
				tasks: ['sass'],
				options: {
					// Start a live reload server on the default port 35729
					livereload: true,
				},
			},
			scripts: {
				files: ['<%= constants.resPath %>/js/**/*.js'],
				tasks: ['uglify'],
				options: {
					debounceDelay: 10,
				},
			},
		},

		clean: {
			js: {
				src: ['<%= constants.publicPath %>/js/**/*.js']
			},
			css: {
				src: ['<%= constants.publicPath %>/**/*.css','<%= constants.publicPath %>/**/*.css.map']
			}
		},

		tinypng: {
			options: {
				apiKey: "PCM6mh_OIxeSWM7comYBSNKzWs-aVp6e",
				checkSigs: true,
				sigFile: '.grunt/tinypng/file_sigs.json',
				summarize: true,
				showProgress: true,
				stopOnImageError: true
			},
			compressResImgs: {
				src: ['<%= constants.publicPath %>/img/**/*.{<%= constants.tinyPngImgFiles %>}'],
				dest: './',
				expand: true,
			}
		}		 
	});

	// Load the plugin that provides the "sass" task.
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-tinypng');

	// Default task(s).
	grunt.registerTask('default', ['watch']);
};
