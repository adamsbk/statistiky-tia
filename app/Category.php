<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public $timestamps = false;
	protected $guarded = [];

	public function food() {
		return $this->belongsToMany('App\Food', 'food_cat');
	}
}
