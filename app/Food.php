<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Food extends Model
{

	protected $table = 'food';
	public $timestamps = false;
	protected $guarded = [];

    /**
	 * The categories that belong to food. Connected by pivot table food_cat
     */
    public function categories() {
    	return $this->belongsToMany('App\Category', 'food_cat');
    }

    public function search($query) {
    	$food = DB::table('food_cat AS fc')
    		->leftJoin('food AS fd', 'fc.food_id', '=', 'fd.id')
    		->leftJoin('categories AS cat', 'fc.category_id', '=', 'cat.id')
    		->select(DB::raw(
    			"fd.*,
    			CONCAT('[', GROUP_CONCAT(CONCAT('{\"id\":',cat.id, ',\"name\":\"', cat.name, '\"}') ORDER BY cat.name), ']') AS categories"
    			));
        if (!empty($query)) {
            $food = $food->where('fd.name', 'like', $query . '%');
        }
        $food = $food
    		->groupBy('fd.id')
    		->get();
    	foreach ($food as $fd) {
    		$fd->categories = json_decode($fd->categories);
    	}
    	return $food;
    }
}
