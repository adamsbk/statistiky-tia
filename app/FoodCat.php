<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodCat extends Model
{
    protected $table = 'food_cat';
	public $timestamps = false;
	protected $guarded = [];
}
