<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use Image;
use App\Category;
use App\Food;
use App\FoodCat;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewInfo = new \stdClass();
        $viewInfo->current = 0;
        $viewInfo->categories = Category::whereNull('parent')
            ->orderBy('name', 'ASC')
            ->get();
        $viewInfo->breadcrumbs = [];
        $viewInfo->food = [];

        return response()->json($viewInfo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function getUniqueImgName() {
        $imgName = null;
        do {
            $imgName = uniqid('img_', true);
        } while (Category::where('img', $imgName)->exists());
        return $imgName;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jsonInput = json_decode($request->input('newCat'), true);
        $cat = new Category($jsonInput);

        if ($request->hasFile('file')) {
            $cat->img = $this->getUniqueImgName();
            $image = $request->file('file');
            $destinationPath = storage_path() . '/uploads';
            $publicPath = public_path() . config('my.img.category_folder');
            try{
                $image->move($destinationPath, $cat->img . '.' . $image->getClientOriginalExtension());
                Image::make($destinationPath . '/' . $cat->img . '.' . $image->getClientOriginalExtension())
                    ->save($publicPath . '/' . $cat->img . config('my.img.extension'))
                    ->fit(config('my.img.medium_width'), config('my.img.medium_height'))->save($publicPath . '/' . $cat->img . config('my.img.medium') . config('my.img.extension'))
                    ->fit(config('my.img.thumb_width'), config('my.img.thumb_width'))->save($publicPath . '/' . $cat->img . config('my.img.thumb') . config('my.img.extension'));
            } catch(\Exception $e) {
                return response()->json(['success' => 0, 'obj' => $e->getMessage()], 400);
            }
        }
        $cat->save();
        return $cat;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $viewInfo = new \stdClass();
        try{
            $viewInfo->current = Category::findOrFail($id);
        } catch(\Exception $e) {
            return response()->json(['success' => 0, 'obj' => $e->getMessage()], 400);
        }
        $viewInfo->categories = Category::where('parent', $id)
                ->orderBy('name', 'ASC')
                ->get();
        $breads = DB::table('categories AS c1')->select(DB::raw('c1.id AS id3, c1.name AS name3, c2.id AS id2, c2.name AS name2, c3.id AS id1, c3.name AS name1'))
            ->leftJoin('categories AS c2', 'c2.id', '=', 'c1.parent')
            ->leftJoin('categories AS c3', 'c3.id', '=', 'c2.parent')
            ->where('c1.id', $id)
            ->first();
        $i = 1;
        while ($i<=3 && is_null($breads->{'id'.$i})) {
            $i++;
        }
        $breadsArr = [];
        while ($i <= 3) {
            $breadsArr[] = (object)['id' => $breads->{'id'.$i}, 'name' => $breads->{'name'.$i}];
            $i++;
        }
        $viewInfo->breadcrumbs = $breadsArr;
        $viewInfo->food = Category::find($id)->food()->orderBy('name')->get();

        return response()->json($viewInfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cat = Category::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json((array) ['success' => 0, 'msg' => $e->getMessage()]);
        }
        if ($cat->img) {
            $publicPath = public_path() . config('my.img.category_folder') . '/' . $cat->img;
            File::delete([
                $publicPath . config('my.img.thumb') . config('my.img.extension'),
                $publicPath . config('my.img.medium') . config('my.img.extension'),
                $publicPath . config('my.img.extension')
            ]);
        }
        return $cat->delete() ? response()->json((array) ['success' => 1]) : response()->json((array) ['success' => 0]);
    }
}
