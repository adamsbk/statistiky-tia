<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use File;
use Image;
use App\Food;
use App\FoodCat;
use App\Customer;
use App\Order;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function getUniqueImgName() {
        $imgName = null;
        do {
            $imgName = uniqid('img_', true);
        } while (Food::where('img', $imgName)->exists());
        return $imgName;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jsonInput = json_decode($request->input('newFood'), true);
        $food = new Food($jsonInput);

        if ($request->hasFile('file')) {
            $food->img = $this->getUniqueImgName();
            $image = $request->file('file');
            $destinationPath = storage_path() . '/uploads';
            $publicPath = public_path() . config('my.img.food_folder');
            try{
                $image->move($destinationPath, $food->img . '.' . $image->getClientOriginalExtension());
                Image::make($destinationPath . '/' . $food->img . '.' . $image->getClientOriginalExtension())
                    ->save($publicPath . '/' . $food->img . config('my.img.extension'))
                    ->fit(config('my.img.medium_width'), config('my.img.medium_height'))->save($publicPath . '/' . $food->img . config('my.img.medium') . config('my.img.extension'))
                    ->fit(config('my.img.thumb_width'), config('my.img.thumb_width'))->save($publicPath . '/' . $food->img . config('my.img.thumb') . config('my.img.extension'));
            } catch(\Exception $e) {
                return response()->json(['success' => false, 'obj' => $e->getMessage()], 400);
            }
        }
        $food->save();
        (new FoodCat(['food_id' => $food->id, 'category_id' => $request->input('catId')]))
            ->save();
        return $food;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $food = Food::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json((array) ['success' => 0, 'msg' => $e->getMessage()]);
        }
        if ($food->img) {
            $publicPath = public_path() . config('my.img.food_folder') . '/' . $food->img;
            File::delete([
                $publicPath . config('my.img.thumb') . config('my.img.extension'),
                $publicPath . config('my.img.medium') . config('my.img.extension'),
                $publicPath . config('my.img.extension')
                ]);
        }
        return $food->delete() ? response()->json((array) ['success' => 1]) : response()->json((array) ['success' => 0]);
    }

    public function addToCategory($id, $catId) {
        try {
            FoodCat::firstOrCreate(['food_id' => $id, 'category_id' => $catId]);
            $fd = Food::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json((array) ['success' => 0, 'msg' => $e->getMessage()]);
        }
        return $fd;
    }

    public function destroyFromCategory($id, $catId) {
        try {
            $row = FoodCat::where('category_id', $catId)
                        ->where('food_id', $id)
                        ->firstOrFail();
        } catch (\Exception $e) {
            return response()->json((array) ['success' => 0, 'msg' => $e->getMessage()]);
        }
        return $row->delete() ? response()->json((array) ['success' => 1]) : response()->json((array) ['success' => 0]);
    }

    public function search($name = null) {
        //return Food::where('name', 'like', $name . '%')->get();
        return (new Food)->search($name);
    }

    public function order(Request $request) {
        $food = $request->get('food');
        if (count($food) > 0 && $request->user()) {
            $newCustomer = new Customer(['user_id' => $request->user()->id, 'order_time' => DB::raw('NOW()')]);
            $newCustomer->save();
            foreach ($food as $fd) {
                $newOrder = new Order([
                    'food_id' => $fd['id'],
                    'customer_id' => $newCustomer->id,
                    'cnt' => (int)$fd['count']==$fd['count'] && (int)$fd['count'] > 0 ? (int)$fd['count'] : 1,
                ]);
                $newOrder->save();
            }
        }
        return response()->json((array) ['success' => 1]);
    }
}
