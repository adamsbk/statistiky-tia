<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( \Auth::user()->hasRole(\Config::get('my.roles.manager')) ) {
            return redirect()->route('manager');
        } else if ( \Auth::user()->hasRole(\Config::get('my.roles.waiter')) ) {
            return redirect()->route('waiter');
        }
        return redirect()->route('login');
        //return view('home');
    }
}
