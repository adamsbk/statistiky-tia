<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Order;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new Order)->maxFoodOrders();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filteredData(Request $request) {
        $maxFoodCnt = $request->get('max_food_cnt');
        if (!is_numeric($maxFoodCnt) || $maxFoodCnt < 1) {
            $maxFoodCnt = 5;
        }
        return (new Order)->maxFoodOrders(
            $maxFoodCnt,
            $request->get('date_from'),
            $request->get('date_to')
        );
    }

    public function foodCntByCategory(Request $request) {
        return (new Order)->foodCntByCategory($request->get('date_from'), $request->get('date_to'));
    }

    public function foodCntPerDay(Request $request) {
        return (new Order)->foodCntPerDay($request->get('date_from'), $request->get('date_to'));
    }
}
