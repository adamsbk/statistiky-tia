<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->hasRole($role)) {
            //if ($request->ajax() || $request->wantsJson()) {
                return response('Forbidden.', 403);
            //} else {
            //    return redirect()->guest('login');
            //}
        }

        return $next($request);
    }
}
