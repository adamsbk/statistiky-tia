<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');

    //kontrolery pre uvodnu stranku manazera a casnika
    Route::get('manager', ['as' => 'manager', 'uses' => 'ManagerController@index']);
    Route::get('waiter', ['as' => 'waiter', 'uses' => 'WaiterController@index']);
});

Route::group(['middleware' => ['api', 'auth.api']], function () {

    Route::get('api/manager/food/search/{name?}','FoodController@search');
    Route::post('api/manager/food/order','FoodController@order');
    
    Route::group(['middleware' => 'role:1'], function() {
        Route::resource('api/manager/food','FoodController');

        Route::resource('api/manager/category','CategoryController');
        Route::post('api/manager/food/{id}/{catId}','FoodController@addToCategory');
        Route::delete('api/manager/food/{id}/{catId}','FoodController@destroyFromCategory');

        Route::post('api/manager/stats/filtered-data','StatisticsController@filteredData');
        Route::post('api/manager/stats/food-cnt-by-cat','StatisticsController@foodCntByCategory');
        Route::post('api/manager/stats/food-cnt-per-day','StatisticsController@foodCntPerDay');
        Route::resource('api/manager/stats', 'StatisticsController');

        Route::resource('api/manager/users', 'ManageUsersController');
    });
});
