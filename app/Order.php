<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Order extends Model
{
    public $timestamps = false;
	protected $guarded = [];

	//vyberie 5 najviac objednávaných jedál + pocet
	public function maxFoodOrders($limit = 5, $dateFrom = null, $dateTo = null) {
		$query = DB::table('orders')
			->select(DB::raw('food.id, food.name, food.img, SUM(orders.cnt) AS cnt'))
    		->leftJoin('food', 'food.id', '=', 'orders.food_id')
    		->groupBy('orders.food_id')
    		->orderBy(DB::raw('cnt'), 'DESC')
    		->take($limit);
    	if ($dateFrom !== null && $dateTo !== null) {
    		$dateFrom = date('Y-m-d 00:00:00', strtotime($dateFrom));
    		$dateTo = date('Y-m-d 23:59:59', strtotime($dateTo));
    		$query
    			->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
    			->whereBetween('customers.order_time', [$dateFrom, $dateTo]);
    	}
    	return $query->get();
	}

    public function foodCntByCategory($dateFrom = null, $dateTo = null) {
        $query = DB::table('orders')
            ->select(DB::raw('categories.id, categories.name, SUM(orders.cnt) AS cnt'))
            ->leftJoin('food_cat', 'orders.food_id', '=', 'food_cat.food_id')
            ->leftJoin('categories', 'categories.id', '=', 'food_cat.category_id')
            ->groupBy('categories.id');
        if ($dateFrom !== null && $dateTo !== null) {
            $dateFrom = date('Y-m-d 00:00:00', strtotime($dateFrom));
            $dateTo = date('Y-m-d 23:59:59', strtotime($dateTo));
            $query
                ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
                ->whereBetween('customers.order_time', [$dateFrom, $dateTo]);
        }
        return $query->get();
    }

    public function foodCntPerDay($dateFrom = null, $dateTo = null) {
        $query =  DB::table('orders')
            ->select(DB::raw('DATE(customers.order_time) AS order_date, SUM(orders.cnt) AS cnt'))
            ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
            ->groupBy(DB::raw('DATE(customers.order_time)'));
        if ($dateFrom !== null && $dateTo !== null) {
            $dateFrom = date('Y-m-d 00:00:00', strtotime($dateFrom));
            $dateTo = date('Y-m-d 23:59:59', strtotime($dateTo));
            $query
                ->whereBetween('customers.order_time', [$dateFrom, $dateTo]);
        }
        return $query->get();
    }
}
