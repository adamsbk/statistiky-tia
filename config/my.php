<?php

return [

	'img' => [
		'extension' => '.jpg',
		'thumb' => '_thumb',
		'medium' => '_medium',
		'thumb_width' => 64,
		'medium_width' => 320,
		'medium_height' => 240,
		'category_folder' => '/cat_img',
		'food_folder' => '/food_img'
	],

    'roles' => [
        'manager' => 1,
        'waiter' => 2,
    ],
    'roles_by_key' => [
        1 => 'Manažér',
        2 => 'Čašník',
    ],
];
