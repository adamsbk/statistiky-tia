
var app =  angular.module('statsApp',['ngRoute', 'ngAnimate', 'ui.bootstrap']);

app.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
		when('/categories/:catId?', {
			templateUrl: 'js/view/categories.html',
			controller: 'categoriesController',
			resolve: {
				data: ['$http', '$route', function($http, $route) {
					var apiUrl = 'api/manager/category';
					if ($route.current.params.catId) {
						apiUrl += '/' + $route.current.params.catId;
					}
					return $http.get(apiUrl)
						.then(function(response) {
							return response.data;
						});
				}]
			}
		}).
		when('/food', {
			templateUrl: 'js/view/food.html',
			controller: 'foodController',
			resolve: {
				data: ['$http', '$route', function($http, $route) {
					return $http.get('api/manager/food/search').then(function(response) {
						return response.data;
					});
				}]
			}
		}).
		when('/stats', {
			templateUrl: 'js/view/stats.html',
			controller: 'statsController',
			resolve: {
				data: ['$http', '$route', function($http, $route) {
					return $http.get('api/manager/stats').then(function(response) {
						return response.data;
					});
				}]
			}
		}).
		when('/users', {
			templateUrl: 'js/view/users.html',
			controller: 'usersController',
			resolve: {
				data: ['$http', '$route', function($http, $route) {
					return $http.get('api/manager/users').then(function(response) {
						return response.data;
					});
				}]
			}
		}).
		otherwise({
			redirectTo: '/food'
		});
	}]);

app.config(['$httpProvider', function($httpProvider) {
	$httpProvider.interceptors.push(['$q', '$window', '$location', function($q, $window, $location) {
		return {
			'responseError': function(rejection) {
				if (rejection.status === 401) {
					$window.location.href = 'login';
				} else if (rejection.status === 403) {
					$window.alert('403 Prístup zamietnutý - presmerujem na Home');
					$location.path('/food');
				}

				return $q.reject(rejection);
			}
		};
	}]);
}]);

app.factory('d3Service', [function(){
	var d3;
	// insert d3 code here
	return d3;
}]);
app.directive('d3Bars', ['d3Service', function(d3Service) {
	return {
		restrict: 'EA',
		scope: {},
		link: function(scope, element, attrs) {
			d3Service.d3().then(function(d3) {
				// our d3 code will go here
				var svg = d3.select(element[0])
					.append("svg")
					.style('width', '100%');
				window.onresize = function() {
					scope.$apply();
				};
				scope.data = [
					{name: "Greg", score: 98},
					{name: "Ari", score: 96},
					{name: 'Q', score: 75},
					{name: "Loser", score: 48}
				];

				// Watch for resize event
				scope.$watch(function() {
					return angular.element($window)[0].innerWidth;
				}, function() {
					scope.render(scope.data);
				});

				scope.render = function(data) {
					// our custom d3 code
				};
			});
		}
	};
}]);

app.run(['$location', '$rootScope', function($location, $rootScope) {
	$rootScope.loading = true;
	$rootScope.$on('$routeChangeStart', function() {
		$rootScope.loading = true;
	});
	$rootScope.$on('$routeChangeSuccess', function() {
		$rootScope.loading = false;
	});
	$rootScope.$on('$routeChangeError', function() {
		$rootScope.loading = false;
	});
}]);