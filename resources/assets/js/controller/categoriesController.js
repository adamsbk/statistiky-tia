app.controller('categoriesController', ['$scope', '$routeParams', '$http', 'data', function($scope, $routeParams, $http, data) {
 
	$scope.categories = null;
	$scope.newCat = {};
	$scope.newFood = {};
	$scope.data = data;
	$scope.autocomplete = {
		food: [],
		text: '',
		notice: {},
	};
	$scope.modalLoading = false;

	$scope.addCategory = function() {
		if ($scope.newCat.name) {
			$scope.modalLoading = true;
			console.log($routeParams.catId);
			if ($routeParams.catId) {
				$scope.newCat.parent = $routeParams.catId;
			}
			var fd = new FormData();
			fd.append('file', $scope.myFile);
			fd.append('newCat', JSON.stringify($scope.newCat));
			$http.post('api/manager/category', fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(response) {
				$scope.modalLoading = false;
				$scope.data.categories.push(response.data);
				$('#add-category-modal').modal('hide');
			}, function errorCallback(response) {
				console.log(response);
			});
		}
	};

	$scope.deleteCategory = function(catId) {
		$http.delete('api/manager/category/' + catId).then(function(response) {
			for(var i = $scope.data.categories.length -1; i >= 0 ; i--) {
				if($scope.data.categories[i].id === catId) {
					$scope.data.categories.splice(i, 1);
				}
			}
		}, function errorCallback(response) {
			console.log(response);
		});
	};

	$scope.addFood = function() {
		if ($scope.newFood.name) {
			$scope.modalLoading = true;
			var fd = new FormData();
			fd.append('file', $scope.myFile);
			fd.append('newFood', JSON.stringify($scope.newFood));
			fd.append('catId', $routeParams.catId);
			$http.post('api/manager/food', fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(response) {
				$scope.modalLoading = false;
				$scope.data.food.push(response.data);
				$('#add-food-modal').modal('hide');
			}, function errorCallback(response) {
				console.log(response);
			});
		}
	};

	$scope.deleteFood = function(foodId) {
		$http.delete('api/manager/food/' + foodId).then(function(response) {
			$scope.autocomplete.notice.alert = 'success';
			$scope.autocomplete.notice.msg = 'Jedlo bolo úspešne odstránené';
			var pos = $scope.autocomplete.food.map(function(e) { return e.id; }).indexOf(parseInt(foodId));
			$scope.autocomplete.food.splice(pos, 1);
			pos = $scope.data.food.map(function(e) { return e.id; }).indexOf(parseInt(foodId));
			if (pos >= 0) {
				$scope.data.food.splice(pos, 1);
			}
		});
	};

	$scope.addFoodToCategory = function(foodId, catId) {
		$http.post('api/manager/food/' + foodId + '/' + catId).then(function(response) {
			var pos = $scope.data.food.map(function(e) { return e.id; }).indexOf(parseInt(foodId));
			if (pos < 0) {
				$scope.autocomplete.notice.alert = 'success';
				$scope.autocomplete.notice.msg = 'Jedlo bolo úspešne pridané';
				$scope.data.food.push(response.data);
			} else {
				$scope.autocomplete.notice.alert = 'warning';
				$scope.autocomplete.notice.msg = 'Jedlo sa už nachádza v kategórii';
			}
		});
	};

	$scope.deleteFoodFromCategory = function(foodId, catId) {
		$http.delete('api/manager/food/' + foodId + '/' + $routeParams.catId).then(function(response) {
			for(var i = $scope.data.food.length -1; i >= 0 ; i--) {
				if($scope.data.food[i].id === foodId) {
					$scope.data.food.splice(i, 1);
				}
			}
		}, function errorCallback(response) {
			console.log(response);
		});
	};

	$scope.autocomplete.search = function() {
		if ($scope.autocomplete.text.length === 0) {
			$scope.autocomplete.loadFood();
			return;
		}
		$http.get('api/manager/food/search/' + $scope.autocomplete.text).then(function(response) {
			$scope.autocomplete.food = response.data;
		});
	}

	//prepopulate food tabe - load only if not in root
	$scope.autocomplete.loadFood = function() {
		if (!$routeParams.catId) {
			return;
		}
		$http.get('api/manager/food/search').then(function(response) {
			if ($routeParams.catId) {
				$scope.autocomplete.food = response.data;
			}
		});
	};
	$scope.autocomplete.loadFood();

	/*$( "#food-autocomplete" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "http://localhost/statistiky/public/api/manager/food/search/" + $('#food-autocomplete').val(),
				dataType: "json",
				success: function( data ) {
					response( data );
				}
			});
		},
		minLength: 1,
		select: function( event, ui ) {
			log( ui.item ?
				"Selected: " + ui.item.label :
				"Nothing selected, input was " + this.value);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li>" )
		.append( '<a>' + item.name + '<br><span class="small">' + item.description + '</a>' )
		.appendTo( ul );
	};*/




	
}])
.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			
			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);




