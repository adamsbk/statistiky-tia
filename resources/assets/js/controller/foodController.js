app.controller('foodController', ['$scope', '$routeParams', '$http', 'data', function($scope, $routeParams, $http, data) {
 
	$scope.autocomplete = {
		food: data,
		text: '',
		notice: {},
	};
	$scope.orders = [];

	$scope.autocomplete.search = function() {
		if ($scope.autocomplete.text.length === 0) {
			$scope.autocomplete.loadFood();
			return;
		}
		$http.get('api/manager/food/search/' + $scope.autocomplete.text).then(function(response) {
			$scope.autocomplete.food = response.data;
		});
	};

	//prepopulate food tabe - load only if not in root
	$scope.autocomplete.loadFood = function() {
		$http.get('api/manager/food/search').then(function(response) {
			$scope.autocomplete.food = response.data;
		});
	};

	$scope.autocomplete.orderFood = function(food) {
		var openedOrders = $scope.orders.filter(function(val) { return val.isOpen});
		openedOrders.map(function(e) {
			//ak sa jedlo nachadza v objednavke, tak zvys len pocet, inak pridaj do objednavky
			var findFood = e.food.filter(function(val) { return val.id === food.id});
			if (findFood.length > 0) {
				findFood[0].count++;
			} else {
				var newFood = angular.copy(food);
				newFood.count = 1; //pocet toho isteho jedla v objednavke
				e.food.push(newFood);
			}
			return e;
		});
	};

	$scope.deleteFood = function(order, index) {
		order.food.splice(index, 1);
	};

	$scope.createOrder = function() {
		var newOrder = {
			id: $scope.orders.length + 1,
			name: $scope.orders.length + 1,
			food: [],
			isOpen: true
		};
		$scope.orders.push(newOrder);
	};

	$scope.postOrder = function(order) {
		$http.post('api/manager/food/order', JSON.stringify(order)).then(function(response) {
			console.log(response.data);
			var pos = $scope.orders.map(function(e) { return e.id; }).indexOf(parseInt(order.id));
			if (pos >= 0) {
				$scope.orders.splice(pos, 1);
			}
		}, function errorCallback(response) {
				console.log(response);
			});
	};

	$scope.cancelOrder = function(order) {
		var pos = $scope.orders.map(function(e) { return e.id; }).indexOf(parseInt(order.id));
		if (pos >= 0) {
			$scope.orders.splice(pos, 1);
		}
	};

}])
.directive('positiveinteger', function(){
	return {
		require: 'ngModel',
		link: function(scope, ele, attr, ctrl){
			ctrl.$parsers.unshift(function(viewValue){
				var intVal = parseInt(viewValue);
				if (isNaN(intVal) || intVal < 1) {
					intVal = 1;
				}
				ele.val(intVal);
				return intVal;
			});
		}
	};
});
