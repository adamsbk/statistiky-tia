app.controller('statsController', ['$scope', '$routeParams', '$http', 'data', function($scope, $routeParams, $http, data) {
 

	$scope.dateOptions = {
		formatYear: 'yy',
		maxDate: new Date(),
		minDate: new Date(2000, 0, 1),
		startingDay: 1
	};
	$scope.format = 'dd.MM.yyyy';

	//one month range
	$scope.dt1 = new Date();
	$scope.dt1.setMonth($scope.dt1.getMonth() - 1);
	$scope.dt2 = new Date();

	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};
	$scope.popup1 = {
		opened: false
	};
	$scope.open2 = function() {
		$scope.popup2.opened = true;
	};
	$scope.popup2 = {
		opened: false
	};

	$scope.maxFoodCnt = 5;

	$scope.maxConsumedFood = data;
	$scope.foodCat = [];
	$scope.foodPerDay = [];

	$scope.getFilteredData = function() {
		var filterData = {
			date_from: $scope.dt1,
			date_to: $scope.dt2,
			max_food_cnt: $scope.maxFoodCnt
		};
		$http.post('api/manager/stats/filtered-data', filterData).then(function(response) {
			//set global data to redraw graphs
			$scope.maxConsumedFood = response.data;
			applePieGraph.change($scope.maxConsumedFood);
		}, function errorCallback(response) {
			console.log(response);
		});
		//bar chart
		$scope.foodCntByCat(filterData);
		//line chart
		$scope.foodCntPerDay(filterData);
	};

	$scope.foodCntByCat = function(filterData) {
		$http.post('api/manager/stats/food-cnt-by-cat', filterData).then(function(response) {
			$scope.foodCat = response.data;
			barChart.change($scope.foodCat);
		});
	};
	$scope.foodCntByCat(null);

	$scope.foodCntPerDay = function(filterData) {
		$http.post('api/manager/stats/food-cnt-per-day', filterData).then(function(response) {
			$scope.foodPerDay = response.data;
			lineChart.change($scope.foodPerDay);
		});
	};
	$scope.foodCntPerDay(null);

	var applePieGraph = new function() {
		var width = 640;
		var height = 350;
		var radius = 140;
		var labelRadius = radius + 8;
		var color = d3.scale.category20();

		var enterAntiClockwise = {
			startAngle: Math.PI * 2,
			endAngle: Math.PI * 2
		};

		var svg = d3.select("#most-popular").append("svg")
		.attr("width", width)
		.attr("height", height)
		.attr('viewBox', '0 0 ' + width + ' ' + height)
		.attr('preserveAspectRatio', 'xMidYMid meet')
		.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		//GAUSIAN BLUR

		var defs = svg.append( 'defs' );

		// append filter element
		var filter = defs.append( 'filter' )
		.attr( 'id', 'dropshadow' ) /// !!! important - define id to reference it later

		// append gaussian blur to filter
		filter.append( 'feGaussianBlur' )
		.attr( 'in', 'SourceAlpha' )
		.attr( 'stdDeviation', 3 ) // !!! important parameter - blur
		.attr( 'result', 'blur' );

		// append offset filter to result of gaussion blur filter
		filter.append( 'feOffset' )
		.attr( 'in', 'blur' )
		.attr( 'dx', 0 ) // !!! important parameter - x-offset
		.attr( 'dy', 0 ) // !!! important parameter - y-offset
		.attr( 'result', 'offsetBlur' );

		// merge result with original image
		var feMerge = filter.append( 'feMerge' );

		// first layer result of blur and offset
		feMerge.append( 'feMergeNode' )
		.attr( 'in", "offsetBlur' )

		// original image on top
		feMerge.append( 'feMergeNode' )
		.attr( 'in', 'SourceGraphic' );

		//END GAUSIAN BLUR

		var arc = d3.svg.arc()
		.outerRadius(radius)
		.innerRadius(radius*0.6);

		var pie = d3.layout.pie()
		.padAngle(.01)
		.value(function(d){ return d.cnt; });

		var g = svg.selectAll(".fan");

		this.change = change;

		function change(newData) {
			var prevG = g;
			g = g.data(pie(newData));
			var gEntries = g.enter().append("g");
			gEntries
				.attr("class", "fan")
				.attr("fill", function(d,i){ return color(i); })
				.on("mouseover", function(d,i) {
					d3.select(this).transition()
						.ease('linear')
						.duration(300)
						.attr('fill', function() { return d3.rgb(color(i)).darker(2); });
					d3.select(this).select('path')
						.attr('filter', 'url(#dropshadow)');
					d3.select('tbody').select('tr:nth-child(' + (i+1) + ')')
						.attr('style', function() { return 'color:#fff;background-color:'+d3.rgb(color(i)).darker(2); });
				})
				.on('mouseout', function(d,i) {
					d3.select(this).transition()
						.ease('linear')
						.duration(300)
						.attr('fill', function() { return color(i); });
					d3.select(this).select('path')
						.attr('filter', 'none');
					d3.select('tbody').select('tr:nth-child(' + (i+1) + ')')
						.attr('style', false);
				})
				.append("path")
				.attr("d", arc(enterAntiClockwise))
				.each(function (d) {
					this._current = {
						data: d.data,
						value: d.value,
						startAngle: enterAntiClockwise.startAngle,
						endAngle: enterAntiClockwise.endAngle
					};
				}); // store the initial values;
			gEntries
				.append('text')
				.attr('class', 'cnt-text')
				.attr('fill', '#fff');
			gEntries
				.append("text")
				.attr('class', 'name-text')
				.attr("dy", ".35em");

			var gExits = g.exit();
			gExits
				.select('path')
				.transition()
				.duration(750)
				.attrTween('d', arcTweenOut);
			gExits
				.transition()
				.delay(750)
				.remove();

			g.select('path')
				.transition()
				.duration(750)
				.attrTween("d", arcTween);

			g.select('.cnt-text')
				.transition()
				.duration(750)
				.attr("transform", function(d){
					return "translate(" + arc.centroid(d) + ")";
				})
				.attr("text-anchor", "middle").text( function(d, i) {
					return d.data.cnt + 'ks';
				});

			g.select(".name-text")
				.transition()
				.duration(750)
				.attr("transform", function(d) {
			        var c = arc.centroid(d),
			            x = c[0],
			            y = c[1],
			            // pythagorean theorem for hypotenuse
			            h = Math.sqrt(x*x + y*y);
			        return "translate(" + (x/h * labelRadius) +  ',' +
			           (y/h * labelRadius) +  ")"; 
			    })
			    .attr("text-anchor", function(d) {
			        // are we past the center?
			        return (d.endAngle + d.startAngle)/2 > Math.PI ?
			            "end" : "start";
			    })
				.text(function(d) { return d.data.name; });
		}

		// Store the displayed angles in _current.
		// Then, interpolate from _current to the new angles.
		// During the transition, _current is updated in-place by d3.interpolate.
		function arcTween(a) {
			var i = d3.interpolate(this._current, a);
			this._current = i(0);
			return function(t) {
				return arc(i(t));
			};
		}
		// Interpolate exiting arcs start and end angles to Math.PI * 2
		// so that they 'exit' at the end of the data
		function arcTweenOut(a) {
			var i = d3.interpolate(this._current, {startAngle: Math.PI * 2, endAngle: Math.PI * 2, value: 0});
			this._current = i(0);
			return function (t) {
				return arc(i(t));
			};
		}
	};
	var barChart = new function() {
		var margin = {top: 20, right: 20, bottom: 150, left: 40},
		width = 600 - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

		var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);

		var y = d3.scale.linear().range([height, 0]);

		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom");

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format("d"))
			.innerTickSize(-width)
			.ticks(10)
			.outerTickSize(0)
			.tickPadding(10);

		var svg = d3.select("#food-cnt-by-cat").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.attr('viewBox', '0 0 ' + (width + margin.left + margin.right) + ' ' + (height + margin.top + margin.bottom))
			.attr('preserveAspectRatio', 'xMidYMid meet')
		.append("g")
			.attr("transform", 
				"translate(" + margin.left + "," + margin.top + ")");

		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")");

		svg.append("g")
			.attr("class", "y axis")
		.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("Počet objednávok");


		function change(data) {			
			x.domain(data.map(function(d) { return d.name; }));
			y.domain([0, d3.max(data, function(d) { return parseInt(d.cnt); })]);

			svg.select('.x.axis').call(xAxis)
			.selectAll("text")
				.style("text-anchor", "end")
				.attr("dx", "-.8em")
				.attr("dy", "-.55em")
				.attr("transform", "rotate(-90)" );
			svg.select('.y.axis').call(yAxis);

			var bars = svg.selectAll(".bar")
				.data(data, function(d) { return d.id; });
			bars.exit()
				.remove();
			bars.enter()
				.append("rect")
				.attr("class", "bar")
				.style("fill", "steelblue");
			bars
				.attr("x", function(d) { return x(d.name); })
				.attr("width", x.rangeBand())
				.attr("y", function(d) { return y(d.cnt); })
				.attr("height", function(d) { return height - y(d.cnt); });

		}
		this.change = change;
	};
	var lineChart = new function() {
		var margin = {top: 20, right: 20, bottom: 30, left: 50},
		width = 960 - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

		var data = [];

		var formatDate = d3.time.format("%Y-%m-%d");
		var bisectDate = d3.bisector(function(d) { return d.order_date; }).left;

		var x = d3.time.scale()
			.range([0, width]);

		var y = d3.scale.linear()
			.range([height, 0]);

		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom");

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format("d"));

		var svg = d3.select("#food-cnt-per-day").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.attr('viewBox', '0 0 ' + (width + margin.left + margin.right) + ' ' + (height + margin.top + margin.bottom))
			.attr('preserveAspectRatio', 'xMidYMid meet')
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")");

		svg.append("g")
			.attr("class", "y axis")
			.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("Počet jedál");

		svg.append("path")
				.attr("class", "line");

		function change(newData) {
			data = newData;
			for (var i=0; i<data.length; i++) {
				data[i].order_date = formatDate.parse(data[i].order_date);
			}

			x.domain(d3.extent(data, function(d) { return d.order_date; }));
			y.domain(d3.extent(data, function(d) { return d.cnt; }));

			var line = d3.svg.line()
				.x(function(d) { return x(d.order_date); })
				.y(function(d) { return y(d.cnt); });

			svg.select('.line')
				.datum(data)
				.attr("d", line);

			svg.select('.x.axis').call(xAxis);
			svg.select('.y.axis').call(yAxis);

		}
		this.change = change;

		var focus = svg.append("g")
			.attr("class", "focus")
			.style("display", "none");

		focus.append("circle")
			.attr("r", 3.5);

		focus.append("text")
			.attr("x", 9)
			.attr("dy", ".35em");

		svg.append("rect")
			.attr("class", "overlay")
			.attr("width", width)
			.attr("height", height)
			.on("mouseover", function() { focus.style("display", null); })
			.on("mouseout", function() { focus.style("display", "none"); })
			.on("mousemove", mousemove);

		function mousemove() {
			var x0 = x.invert(d3.mouse(this)[0]),
			i = bisectDate(data, x0, 1),
			d0 = data[i - 1],
			d1 = data[i],
			d = x0 - d0.order_date > d1.order_date - x0 ? d1 : d0;
			focus.attr("transform", "translate(" + x(d.order_date) + "," + y(d.cnt) + ")");
			focus.select("text").text(d.cnt + 'ks');
		}
	};
	applePieGraph.change($scope.maxConsumedFood);
}]);