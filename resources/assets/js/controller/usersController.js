app.controller('usersController', ['$scope', '$routeParams', '$http', 'data', function($scope, $routeParams, $http, data) {
 
	$scope.users = data;
	for (var i=0; i<$scope.users.length; i++) {
		$scope.users[i].role = parseInt($scope.users[i].role);
		$scope.users[i].active = parseInt($scope.users[i].active);
	}

	$scope.changeUsersRole = function(user) {
		$http.put('api/manager/users/'+user.id, user).then(function(response) {
			
		}, function errorCallback(response) {
			console.log(response);
		});
	};

}]);
