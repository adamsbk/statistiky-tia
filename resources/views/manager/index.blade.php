<!-- resources/views/manager/index.blade.php -->

@extends('layouts.app')

@section('content')

<div class="container" ng-app="statsApp" id="stats-app">
    <ng-view></ng-view>
    <p ng-bind="data.current.name"></p>
    <div class="loading" ng-show="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>

@endsection